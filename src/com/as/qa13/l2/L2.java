package com.as.qa13.l2;

import java.util.Scanner;

public class L2 {

    public static void main(String[] args){

        int i;
        short s, s1;
        byte b;

        int i2 = 0;
        i = 0;

        s = 0x10;   // 16
        s = 0xF;    // Hex 15
        s = 010;    //Oct 8

        s = 0b111;    // Bin 7

        char ch = 'v';   // char v
        ch = '\t';      // Tab
        ch = '\n';
        ch = '\u0056';
        ch = 30;

        i = 100_000;

        float f = 3.2f;
        double d = 3.2;

        boolean v = f > d;

        long l = 10l;


        f = 1.3e10f;

        ch = '\"';

        String str = "Hello \"World\"";

        boolean bb = true;
        boolean bb2 = false;

        System.out.print(bb);
        System.out.println(str);

        Scanner scan = new Scanner(System.in);
        int ii = scan.nextInt();

        System.out.println(ii);
    }


}
