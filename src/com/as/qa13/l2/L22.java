package com.as.qa13.l2;

public class L22 {

    public static void main(String[] args){
        int i = 5 + 6;
        int a = i + 2;

        int i2 = i + a;

        int i3 = i * 2;

        // Compare

        boolean b1, b2, b3;

        b1 = true;
        b2 = false;

        b3 = b1 && b2;
        System.out.println(b3);
        b3 = b1 || b2;
        System.out.println(b3);
        b3 = !b1;
        System.out.println(b3);

        b3 = b1 == b2;
        System.out.println(b3);
        b3 = b1 != b2;
        System.out.println(b3);

    }
}
